.. em-bright documentation master file, created by
   sphinx-quickstart on Thu Sep  2 16:16:41 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to em-bright's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   em_bright
   compute_disk_mass


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
